"use strict";
// Simple class to check whether import/export is working
var Vehicle = (function () {
    function Vehicle(type, wheels, canFly, manufacturer) {
        this.type = type;
        this.wheels = wheels;
        this.canFly = canFly;
        this.manufacturer = manufacturer;
    }
    Vehicle.prototype.getType = function () { return this.type; };
    Vehicle.prototype.getWheels = function () { return this.wheels; };
    Vehicle.prototype.getCanFly = function () { return this.canFly; };
    Vehicle.prototype.getManufacturer = function () { return this.manufacturer; };
    Vehicle.prototype.setType = function (type) { this.type = type; };
    Vehicle.prototype.setWheels = function (wheels) { this.wheels = wheels; };
    Vehicle.prototype.setCanFly = function (canFly) { this.canFly = canFly; };
    Vehicle.prototype.setManufacturer = function (manufacturer) { this.manufacturer = manufacturer; };
    Vehicle.prototype.determineArticle = function () {
        if (this.type[0].match(/[aeiou]/g)) {
            return 'an';
        }
        else {
            return 'a';
        }
    };
    Vehicle.prototype.boolToString = function () {
        if (this.canFly) {
            return 'an aerial vehicle';
        }
        else {
            return 'a ground vehicle';
        }
    };
    Vehicle.prototype.numberToString = function () {
        var numbers = ['no', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
        if (this.wheels === 0)
            return numbers[this.wheels];
        else if (this.wheels === 2)
            return numbers[this.wheels];
        else if (this.wheels === 3)
            return numbers[this.wheels];
        else if (this.wheels === 4)
            return numbers[this.wheels];
        else if (this.wheels === 5)
            return numbers[this.wheels];
        else if (this.wheels === 6)
            return numbers[this.wheels];
        else if (this.wheels === 7)
            return numbers[this.wheels];
        else if (this.wheels === 8)
            return numbers[this.wheels];
        else if (this.wheels === 9)
            return numbers[this.wheels];
        else
            return this.wheels;
    };
    Vehicle.prototype.printMe = function () {
        console.log("\n        You instantiated " + this.determineArticle() + " " + this.type + ".\n        It has " + this.numberToString() + " wheels.\n        It is " + this.boolToString() + ".\n        The manufacturer of the vehicle is " + this.manufacturer + ".\n        ");
    };
    return Vehicle;
}());
exports.Vehicle = Vehicle;
