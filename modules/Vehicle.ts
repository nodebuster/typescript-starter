// Simple class to check whether import/export is working
export class Vehicle {
    type: string;
    wheels: number;
    canFly: boolean;
    manufacturer: string;

    constructor(type, wheels, canFly, manufacturer) {
        this.type = type;
        this.wheels = wheels;
        this.canFly = canFly;
        this.manufacturer = manufacturer;
    }

    getType(): string { return this.type; }
    getWheels(): number { return this.wheels; }
    getCanFly(): boolean { return this.canFly; }
    getManufacturer(): string { return this.manufacturer; }

    setType(type: string) { this.type = type; }
    setWheels(wheels: number) { this.wheels = wheels; }
    setCanFly(canFly: boolean) { this.canFly = canFly; }
    setManufacturer(manufacturer: string) { this.manufacturer = manufacturer; }

    determineArticle(): string {
        if (this.type[0].match(/[aeiou]/g)) {
            return 'an';
        } else {
            return 'a';
        }
    }

    boolToString(): string {
        if (this.canFly) {
            return 'an aerial vehicle';
        } else {
            return 'a ground vehicle';
        }
    }

    numberToString(): string | number {
        let numbers = ['no', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
        if (this.wheels === 0) return numbers[this.wheels];
        else if (this.wheels === 2) return numbers[this.wheels];
        else if (this.wheels === 3) return numbers[this.wheels];
        else if (this.wheels === 4) return numbers[this.wheels];
        else if (this.wheels === 5) return numbers[this.wheels];
        else if (this.wheels === 6) return numbers[this.wheels];
        else if (this.wheels === 7) return numbers[this.wheels];
        else if (this.wheels === 8) return numbers[this.wheels];
        else if (this.wheels === 9) return numbers[this.wheels];
        else return this.wheels;
    }

    printMe() {
        console.log(`
        You instantiated ${this.determineArticle()} ${this.type}.
        It has ${this.numberToString()} wheels.
        It is ${this.boolToString()}.
        The manufacturer of the vehicle is ${this.manufacturer}.
        `)
    }
}