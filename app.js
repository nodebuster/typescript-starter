"use strict";
// A starter template for Typescript development
var PrintMe_1 = require("./modules/PrintMe");
var Vehicle_1 = require("./modules/Vehicle");
// Testing printing
PrintMe_1.printMe('Hello');
PrintMe_1.printMe(100);
PrintMe_1.printMe(true);
PrintMe_1.printMe("\n**************************************************************");
// Testing instantiating class
var motorCycle = new Vehicle_1.Vehicle('motorcycle', 2, false, 'Honda');
motorCycle.printMe();
PrintMe_1.printMe("**************************************************************");
var car = new Vehicle_1.Vehicle('car', 4, false, 'Lada');
car.printMe();
PrintMe_1.printMe("**************************************************************");
var aeroplane = new Vehicle_1.Vehicle('aeroplane', 10, true, 'Airbus');
aeroplane.printMe();
PrintMe_1.printMe("**************************************************************\n");
var submarine = new Vehicle_1.Vehicle('submarine', 0, false, 'CCCP');
submarine.printMe();
PrintMe_1.printMe("**************************************************************\n");
var tricycle = new Vehicle_1.Vehicle('tricycle', 3, false, 'Toys\'r\'Us');
tricycle.printMe();
PrintMe_1.printMe("**************************************************************\n");
