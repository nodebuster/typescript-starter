// A starter template for Typescript development
import {printMe} from './modules/PrintMe';
import {Vehicle} from './modules/Vehicle';

// Testing printing
printMe('Hello');
printMe(100);
printMe(true);
printMe(`
**************************************************************`);

// Testing instantiating class
let motorCycle = new Vehicle('motorcycle', 2, false, 'Honda');
motorCycle.printMe();
printMe(`**************************************************************`);

let car = new Vehicle('car', 4, false, 'Lada');
car.printMe();
printMe(`**************************************************************`);

let aeroplane = new Vehicle('aeroplane', 10, true, 'Airbus');
aeroplane.printMe();
printMe(`**************************************************************
`);

let submarine = new Vehicle('submarine', 0, false, 'CCCP');
submarine.printMe();
printMe(`**************************************************************
`);

let tricycle = new Vehicle('tricycle', 3, false, 'Toys\'r\'Us');
tricycle.printMe();
printMe(`**************************************************************
`);
